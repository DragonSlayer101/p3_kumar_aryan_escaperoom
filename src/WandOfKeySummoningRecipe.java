
public class WandOfKeySummoningRecipe extends Recipe {

	public WandOfKeySummoningRecipe() {
		super(false, new String[]{"runed_stick", "phoenix_feather", "saphire", "unicorn_tears"});
	}

	@Override
	public void combineInRoom(Room room) {
		System.out.println("The ingredients combine together and a wand_of_key_summoning appears in your room.");
		room.add(new WandOfKeySummoning("wand_of_key_summoning", "A wand that can summon keys"));
		removeIngredientsFromRoom(room);
	}

}
