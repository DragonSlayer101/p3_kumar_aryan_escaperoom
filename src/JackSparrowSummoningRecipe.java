
public class JackSparrowSummoningRecipe extends Recipe {

	public JackSparrowSummoningRecipe() {
		super(false, new String[]{"long_braided_hair", "johnny_depp", "pirate_hat"});
	}
	
	public void combineInRoom(Room room) {
		System.out.println("The ingredients combine together and Jack Sparrow appears in your room.");
		room.add(new UselessItem("jack_sparrow", "The captain of the ship"));
		removeIngredientsFromRoom(room);
	}
}
