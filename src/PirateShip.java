
public class PirateShip extends Room {
	/** max number of turns the player can take before failing */
	private int maxTurns;

	/** the number of turns the player has taken */
	private int numTurns;

	public PirateShip(String description, String intro, int maxTurns) {
		super(description, intro);
		add(new JackSparrowSummoningRecipe());
		Container chest1 = new Container("gold_chest", "A chest with messages inside.");
		TextItem paper = new TextItem("paper", "A paper with a message written in it", 
				"To all ye that have made it here\n" + 
				"Ye have made it far, but to steal\n" + 
				"Me loot, you must show that ye know\n" +
				"The name of greatest pirate crook:\n"+
				"Captain **** *******");
		TextItem password = new TextItem("password", "A paper with the password to the hint chest", 
				"Ye be warned. I shall give ye a hint\n" + 
				"but not without a price. The password\n" + 
				"to the hint chest is black pearl. If ye\n" + 
				"choose to open the hint chest, ye shall\n" +
				"pay the price");
		chest1.add(password);
		chest1.add(paper);
		add(chest1);
		
		Container chest2 = new PasswordLockedContainer("hint_chest", "A chest with a hint inside.", "black pearl");
		UselessItem curse = new UselessItem("curse", "A curse placed on you. Each turn is now 2 turns");
		TextItem curseMessage = new TextItem("curse_message", "A paper explaining the curse", 
				"I told ye I would trick ye\n" + 
				"Now ye have half as much time\n" + 
				"As 1 turn now be 2 turns");
		TextItem hint = new TextItem("hint", "A paper with a hint", 
				"The greatest pirate crook is from the\n" + 
				"Caribbean. But now, ye has payed the price\n" + 
				"Ye got a curse by opening the chest.");
		chest2.add(curse);
		chest2.add(curseMessage);
		chest2.add(hint);
		add(chest2);
		
		Container chest3 = new PasswordLockedContainer("wooden_chest", "A wooden chest decorated with skulls.", "Jack Sparrow");
		chest3.add(new UselessItem("long_braided_hair", "The long braided hair of Jack Sparrow"));
		chest3.add(new UselessItem("johnny_depp", "The person who acts as Jack Sparrow"));
		chest3.add(new UselessItem("pirate_hat", "The hat of Jack Sparrow"));
		add(chest3);
		
		this.numTurns = 0;
		this.maxTurns = maxTurns;
	}

	@Override
	public void printRoomPrompt() {
		System.out.println("You have taken " + numTurns + " turns. You have " + (maxTurns - numTurns) + " turns left to escape.");
	}

	@Override
	public void onCommandAttempted(String command, boolean handled) {
		if (handled) {
			numTurns++;
			if (getItem("curse") != null) numTurns++;
		}
	}

	@Override
	public boolean escaped() {
		return getItem("jack_sparrow") != null;
	}

	@Override
	public void onEscaped() {
		System.out.println("Jack Sparrow has been revived and he opens the door of the ship for you!\n" + 
				"He gives you the golden dubloons and you leave the sinking ship.\n" + 
				"Congratulations, you have escaped in " + numTurns + " turns!");
	}

	@Override
	public void onFailed() {
		System.out.println("Oh no! The ship has sunk with you in it.");
		System.out.println("Game Over");
	}

	@Override
	public boolean failed() {
		return numTurns >= maxTurns;
	}
}
