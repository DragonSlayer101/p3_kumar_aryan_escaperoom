/*
 * Aryan Kumar. Period 3. February 20.
 * 3-4 hours.
 * This lab was fun but it did take a long time. It took me a long time to set up my account and
 * download a git. I was not present at class since I had a fever so I had to figure how to do that
 * portion on my own. After that, it was not too hard. It would have been better if some of the code
 * was already copy-pasted into the package that we had to download so that could have saved time.
 * The instructions were a bit long too. The lab went well for me. It was nice to design a text
 * adventure game, particularly one with a lot of parts. It was nice to see how the framework
 * that we designed for the wizard game could be easily modified to work for a different game as well.
 */
public class PirateShipDriver {

	public static void main(String[] args) {
		String intro = "Welcome to the Pirate Ship!\n" +
			 	 "You have been looking for the pirate ship of the greatest pirate\n" +
			 	 "to ever sail the seven seas. Today was your lucky day and you\n" +
			 	 "found it. Once inside, you enter a room stashed with golden dubloons.\n" +
			 	 "When you pick one up, the door shuts and water starts to poor into\n" +
			 	 "the ship. You must escape before water fills the ship and it sinks.\n" +
			 	 "Get what you need and get out!";
		PirateShip ship = new PirateShip("This is a pirate's ship.", intro, 25);
		new EscapeApp(ship).runGame();
	}

}
