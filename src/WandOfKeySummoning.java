
public class WandOfKeySummoning extends Item {

	public WandOfKeySummoning(String name, String description) {
		super(name, description);
	}

	@Override
	public void use() {
		System.out.println("A golden key has appeared in your room. It appears to be the key that will unlock the magical door.");
		getRoom().add(new UselessItem("gold_key", "an item that can open the door"));
	}

}
